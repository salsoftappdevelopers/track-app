import createDataContext from './CreateDataContext';

const locationReducer =(state,action)=>{
     switch(action.type)
     {

          case 'change_name':
               return{...state, name : action.payload};
               case 'start_record':
                 return {...state,recording : true};
                 case 'stop_record':
                    return {...state,recording : false};
         default :
         return state;
     }
};

const startRecording = dispatch =>() => {
     dispatch({type : 'start_record'});
};
const stopRecording = dispatch =>() => {
     dispatch({type : 'stop_record'});
};

const addLocation = dispatch =>() => {

};

const changeName = dispatch => (name)=>{
         dispatch({type : 'change_name',payload : name});
};

export const {context, provider} =createDataContext(
     locationReducer,
     {startRecording,stopRecording,addLocation,changeName},
     {name : '',recording : false, locations : [] ,currentlocation : null}
);