import AsyncStorage from '@react-native-community/async-storage';
import createdatacontext from './CreateDataContext';
import trackerApi from '../api/tracker';


const authReducer = (state,action)=>{
       switch(action.type){

        case 'add_error':
            return {...state,errormessage : action.payload};

            case 'signin':
                return{errormessage : '',token: action.payload};
            case 'clear_error_message':
                return{...state,errormessage : ''};

            case 'sign out':
                return {token : null,errormessage :''};
                      
             default:
                 return state;
       }
};

const clearErrorMessage = dispatch =>{
      return () =>{
        dispatch({type :'clear_error_message'});
      }
      
};

const tryLocalSignin= (dispatch) =>{

    return async(callback)=>{

        const token=await AsyncStorage.getItem('token');
        console.log(AsyncStorage.getItem('token'));
        if(token)
        {
            console.log("true");
            
          dispatch({type:'signin',payload: response.data.token});
          console.log("theer");
          callback()
        }
        else{
            
        }
    };
    
};

const signup=dispatch => {
      return async ({email,password},callback)=>{
          
        try{
 
            const response=await trackerApi.post('/signup',{email,password});
           await AsyncStorage.setItem('token',response.data.token);
           console.log(response.data);
           console.log(AsyncStorage.getItem('token'));
           dispatch({type:'signin',payload: response.data.token});
        //    navigate('Track List');
            callback()
        }catch(err){
            console.log("error",err);
            dispatch({type:'add_error' ,payload :"something went wrong with signup"});
        }
      };
};

const signin=(dispatch)=>
{
    return async ({email,password},callback)=>{
          
        try{
 
            const response=await trackerApi.post('/signin',{email,password});
           await AsyncStorage.setItem('token',response.data.token);
           console.log(response.data);
           console.log(AsyncStorage.getItem('token'));
           dispatch({type:'signin',payload: response.data.token});
        //    navigate('Track List');
            callback()
        }catch(err){
            console.log("error",err);
            dispatch({type:'add_error' ,payload :"something went wrong with signin"});
        }
      };
};

const signout=(dispatch)=>
{
    return async ()=>{
        await AsyncStorage.removeItem('token');
         dispatch({type: "sign out"});
         console.log("token remove",token)
    }
     
};


export const {provider,context}=createdatacontext(
    authReducer,
    {signin,signout,signup,clearErrorMessage,tryLocalSignin},
    {token:null,errormessage: ''}
);

// export const name ="john"

// export const age = 2