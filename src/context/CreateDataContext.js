import React, { useReducer } from 'react';

const createContext  =  (reducer,action,defaultvalue)=>{
       const context=React.createContext();

       const provider=({children})=>{
               const[state,dispatch]=useReducer(reducer,defaultvalue);
               const boundactions={};
               for(let key in action)
               {
                   boundactions[key]=action[key](dispatch);
               }

               return(
                   <context.Provider value={{state,...boundactions}}>
                       {children}
                   </context.Provider>
               );

       };

       const obj = {
        context,
         provider
       }

       return obj;
};

export default createContext;