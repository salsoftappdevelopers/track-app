import React from 'react';
import { View,Text,TouchableOpacity,StyleSheet} from 'react-native';
import Spacer from '../components/spacer';

const Navlink =({navigation,routename,text})=>
{
      return(
          <View>
               <TouchableOpacity onPress={()=> { navigation.navigate(routename) }}>
 
                    <Spacer>
                    <Text style={styles.signinStyle}>{text}</Text>
                    </Spacer>

                    </TouchableOpacity>
          </View>
      );
};

const styles=StyleSheet.create({
    container:{
       flex:1,
       
       justifyContent:"center",
       marginBottom:100
    },

    errorstyle:{
           color: 'red',
           fontSize:15,
           marginLeft:15
    },
    signinStyle:{
        color:'blue'
    }
});

export default Navlink;