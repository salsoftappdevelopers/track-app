import React, { useState,useContext } from 'react';
import { View,StyleSheet } from 'react-native';
import { Input,Text, Button} from 'react-native-elements';
import Spacer from '../components/spacer';

const AuthForm=( {navigation,headerText,errormessage,onSubmit,submitButtontext})=>{

    const [email,setEmail]=useState('');
    const[password,setPassword]=useState('');

  return(
             <View>
                   <Spacer>
              <Text h3>{headerText}</Text>   
             </Spacer>
             <Spacer>
             <Input label="Email"
                    value={email}
                    onChangeText={(newemail)=> setEmail(newemail)}
                    autoCapitalize="none"
                    autoCorrect={false}></Input>  
             </Spacer>
            
             <Spacer>
             <Input 
                     secureTextEntry
                     label="Password"
                     value={password}
                     onChangeText={(newpassword)=> setPassword(newpassword)}
                     autoCapitalize="none"
                     autoCorrect={false}></Input>   
             </Spacer> 

         {errormessage? (
          <Text style={styles.errorstyle}>{errormessage}</Text>
          ): null} 
             <Spacer>
             <Button title={submitButtontext}
                      onPress={() => onSubmit({email,password},()=>{
                          navigation.navigate("Home")
                      })}></Button>
                 </Spacer>
             </View>
  );
};
const styles =StyleSheet.create({
    
    errorstyle:{
        color: 'red',
        fontSize:15,
        marginLeft:15
 },
});

export default AuthForm;