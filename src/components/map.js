import { Input, Text } from 'react-native-elements';
import React, { useState, useEffect, useContext } from 'react';
import MapView, { Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import { View, StyleSheet, TouchableOpacity, PermissionsAndroid, Button, Platform } from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import { Marker } from 'react-native-maps';

const map = ({ navigation }) => {
  let points = [];
  const [
    currentLongitude,
    setCurrentLongitude
  ] = useState(67.0672758);
  const [
    currentLatitude,
    setCurrentLatitude
  ] = useState(24.8739707);

 const start_Point= {
  latitude: 24.8739707,
  longitude: 67.0672758
 }

 console.log("start", start_Point);

 const end_points={
  latitude: currentLatitude,
  longitude: currentLongitude
}

console.log("end",end_points);

  console.log("latitude", currentLatitude);
  console.log("longitude", currentLongitude);

  //   function success(position) {
  //     var crd = position.coords;

  //     if (target.latitude === crd.latitude && target.longitude === crd.longitude) {
  //       console.log('Congratulations, you reached the target');
  //       navigator.geolocation.clearWatch(id);
  //     }
  //   }

  //   function error(err) {
  //     console.warn('ERROR(' + err.code + '): ' + err.message);
  //   }

  //   target = {
  //     latitude : currentLatitude,
  //     longitude: currentLongitude
  //   };

  //   options = {
  //     enableHighAccuracy: false,
  //     timeout: 5000,
  //     maximumAge: 0
  //   };

  const getTracks = () => {
    Geolocation.watchPosition(position => {
      let currentLongitude = position.coords.longitude;


      //getting the Latitude from the location json
      let currentLatitude = position.coords.latitude;
      setCurrentLongitude(currentLongitude);

      // //     //Setting Longitude state
      setCurrentLatitude(currentLatitude);
      //setPoints([{latitude: setCurrentLatitude(currentLatitude),longitude: setCurrentLongitude(currentLongitude)}])
      try {
        //points.push(position.coords);
        const end_points={
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        }
        //points.push(position.coords[longitude]);
      } catch (err) {
        console.log("error", err);
      }

      console.log("points", points);
      console.log("position", position);

    }, (error) => {
      console.log(error);
    },
      {
        enableHighAccuracy: true,
        // timeout: 30000,
        // maximumAge: 1000
      },
    );
  };

  // useEffect(()=>{
  //      getTracks();
  // }
  // ,[]);

  useEffect(() => {

    if (Platform.OS === 'android') {
      requestLocationPermission();
    }
    getTracks();

    //return () => clearInterval(interval);
  }, []);

  const getonetimelocation = () => {

    //const id = Geolocation.watchPosition(success, error, options);
    //console.log("id",id);

  };


  const requestLocationPermission = async () => {

    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {

        }
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("permission access");
      }
      else {
        console.log("permission not access");
      }


    } catch (err) {
      console.log("error", err);
    }
  };
  const state = {
    location: null
  };



  // const findCoordinates = () => {
  // 	navigation.geolocation.getCurrentPosition(
  // 		position => {
  // 			const location = JSON.stringify(position);

  // 			this.setState({ location });
  // 		},
  // 		error => Alert.alert(error.message),
  // 		{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
  // 	);
  // };



  return (
    <View>
      <MapView style={styles.Mapstyle}
        provider={PROVIDER_GOOGLE}
        zoomEnabled={true}
        
        initialRegion={
          {
            latitude: currentLatitude,
            longitude: currentLongitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          }


        } showsUserLocation={true}
        >
        <Marker
          coordinate={{ latitude: currentLatitude, longitude: currentLongitude }}
          title="this is a marker"
          description="this is a marker example"
        />
        <Polyline coordinates={[start_Point,{ latitude: currentLatitude, longitude: currentLongitude }]} />
      </MapView>

      {/* <TouchableOpacity onPress={getonetimelocation}>
					<Text style={styles.welcome}>Find My Coords?</Text>
					<Text>Location: {state.location}</Text>
				</TouchableOpacity>
                <Button title="click for permission"
                         onPress={requestLocationPermission}/> */}
    </View>

  );
};
const styles = StyleSheet.create({
  Mapstyle: {
    flex :1,
    height: 500
  }
});
export default map;