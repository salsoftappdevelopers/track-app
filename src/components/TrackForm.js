import React, { useState } from 'react';
import { Input,Text, Button} from 'react-native-elements';



const TrackForm =({recording,startRecord,stopRecord})=>{

    //const {changeName,startRecording,stopRecording} = useContext(LocationContext);
    //const { name ,setName} =useState('');
    //console.log(state);
   // const { startRecording,stopRecording }=useContext(LocationContext);
     
    const [name,setName]= useState('');
    console.log("name",name);
       return(
           <>
           
                 
                <Input 
                value={name}
                placeholder="enter title"
                 onChangeText={newValue=> setName(newValue)}
                />
               
                {recording
                ? <Button title="Stop Recording" onPress={stopRecord}/>
                : <Button title="Start Recording" onPress={startRecord}/>
                }

             
              
           </>
       );
};

export default TrackForm;