import React, { useContext } from 'react';
import { View,Text,Button} from 'react-native';
import {context as AuthContext} from '../context/AuthContext';
import Spacer from '../components/spacer';

const AccountScreen=({navigation})=>
{

    const {signout}=useContext(AuthContext);

   return(
         <View>
             <Text style={{fontSize:40}}>Account Screen</Text>
             <Spacer>
                 <Button title="Sign out"
                          onPress={()=>{signout()}}
                        //onPress={()=>{signout(()=>alert())}}
                         >
                         
                 </Button>
             </Spacer>
         </View>
   );
};
export default AccountScreen;