import React, { useState,useEffect,useContext } from 'react';
import {context as AuthContext} from '../context/AuthContext';

const ResolveAuthScreen = ({navigation}) =>
{
        const {tryLocalSignin}=useContext(AuthContext);

        useEffect(()=>{
              tryLocalSignin(()=>{navigation.navigate("Track List")}); 
        },[]);
        return null;
};

export default ResolveAuthScreen;