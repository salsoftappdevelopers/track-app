import React from 'react';
import { View,Text,StyleSheet} from 'react-native';
import AuthForm from '../components/AuthForm';
import Navlink from '../components/Navlink';
import {context as AuthContext} from '../context/AuthContext';
import { useContext,useEffect } from 'react';


const SigninScreen=({navigation})=>
{
    const {state,signin,clearErrorMessage}=useContext(AuthContext);
    console.log("sign context",useContext(AuthContext));
    useEffect(()=>{
          navigation.addListener('blur',()=> {
            clearErrorMessage()
          });
    },[]);

   return(
                <View style={styles.container}>
                        
                <AuthForm headerText="Sign In for Tracker"
                            errormessage={state.errormessage}
                            submitButtontext="Sign in"
                            onSubmit={signin}
                            navigation={navigation}/>
                
                
            <Navlink 
            navigation={navigation}
            routename="Sign up"
                        text="Dont have an account ? Sign up instead"/>

            </View>
   );
};

const styles=StyleSheet.create({
    container:{
       flex:1,
       
       justifyContent:"center",
       marginBottom:100
    },

    errorstyle:{
           color: 'red',
           fontSize:15,
           marginLeft:15
    },
    signinStyle:{
        color:'blue'
    }
});
export default SigninScreen;