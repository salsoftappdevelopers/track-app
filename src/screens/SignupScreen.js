import React, { useState,useEffect,useContext } from 'react';
import { View,StyleSheet } from 'react-native';
import { Input,Text, Button} from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Spacer from '../components/spacer';
import {context as AuthContext} from '../context/AuthContext';
import AuthForm from '../components/AuthForm';
import Navlink from '../components/Navlink';


const SignupScreen=({navigation})=> {

   
    const {state,signup,clearErrorMessage}=useContext(AuthContext);
    useEffect(()=>{
        navigation.addListener('blur',()=> {
          clearErrorMessage()
        });
  },[]);



   return(
         <View style={styles.container}>
             
                 <AuthForm headerText="Sign Up for Tracker"
                              errormessage={state.errormessage}
                              submitButtontext="Sign up"
                              onSubmit={signup}
                              navigation={navigation}/>
                  
                    
                <Navlink 
                navigation={navigation}
                routename="Sign in"
                         text="Already have an account ? Sign in instead"/>
            
         </View>
           
   );

};

const styles=StyleSheet.create({
    container:{
       flex:1,
       
       justifyContent:"center",
       marginBottom:100
    },

    errorstyle:{
           color: 'red',
           fontSize:15,
           marginLeft:15
    },
    signinStyle:{
        color:'blue'
    }
});
export default SignupScreen;