import React from 'react';
import { View,Text, Button} from 'react-native';

const TrackList=({navigation})=>
{
   return(
         <View>
             <Text style={{fontSize:40}}>Account Screen</Text>
             <Button title="Go to Track detail"
                     onPress={()=>{navigation.navigate("Track detail")}}></Button>
         </View>
   );
};
export default TrackList;