import { CommonActions, StackActions } from "@react-navigation/native";

let navigator;

export const setNavigator = (nav) => {
    navigator = nav;
    console.log('nav : ',nav)
};

export const navigate = (routeName, params) => {
    navigator.dispatch(
        CommonActions.navigate({
            name: routeName,
            key: routeName + new Date(),
            params
        })
    );
};

