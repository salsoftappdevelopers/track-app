/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useContext } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import SigninScreen from './src/screens/SigninScreen';
import SignupScreen from './src/screens/SignupScreen';
import TrackList from './src/screens/TrackList';
import TrackCreateScreen from './src/screens/TrackCreateScreen';
import AccountScreen from './src/screens/AccountScreen';
import TrackDetailscreen from './src/screens/TrackDetailscreen';
import { provider as AuthProvider } from './src/context/AuthContext';
import {context as AuthContext} from './src/context/AuthContext';
import {Provider as LocationProvider}  from './src/context/locationContext';


const SettingsStack = createStackNavigator();
const HomeStack = createStackNavigator();
const Account = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const App = () => {

  
  return (

       
         <AuthProvider>
      <NavigationContainer >
        <RootNavigator />
      </NavigationContainer>
    </AuthProvider>
    
  
  )
};

{/*     
           <Tab.Navigator>
          <Tab.Screen name="Tracklistflow">
            {() => (
              <SettingsStack.Navigator >
                <SettingsStack.Screen
                  name="Track list"
                  component={TrackList}
                />
                <SettingsStack.Screen name="Track detail" component={TrackDetailscreen} />
              </SettingsStack.Navigator>
            )}
          </Tab.Screen>
          <Tab.Screen name="Track Create">
            {() => (
              <HomeStack.Navigator>
                <HomeStack.Screen name="Track create" component={TrackCreateScreen} />
              
              </HomeStack.Navigator>
            )}
          </Tab.Screen>
          <Tab.Screen name="Account">
            {() => (
              <HomeStack.Navigator>
                <HomeStack.Screen name="Account" component={AccountScreen} /> 
              </HomeStack.Navigator>
            )}
          </Tab.Screen>
        </Tab.Navigator>  */}
// class RootNavigator extends React.Component {

  
 
//     return (
 
      
     
//     );
  
// }

const RootNavigator =()=>
{

  const {state}=useContext(AuthContext);
  let token=state.token;
  console.log("token store",token);
    return(
      < Stack.Navigator
      // initialRouteName="Sign up"
    >
 
         {  
        
         token ? <Stack.Screen name="Home" component={Root}></Stack.Screen> 
           
         : <><Stack.Screen name="Sign up" component={SignupScreen}></Stack.Screen>
           <Stack.Screen name="Sign in" component={SigninScreen}></Stack.Screen>
          </>
          
        
  }        
        
      
      {/* <Stack.Screen name="Auth" component={ResolveAuthScreen}></Stack.Screen>
      <Stack.Screen name="Account" component={AccountScreen}></Stack.Screen> */}
    </Stack.Navigator >

    );
}


const Root = () => {
  return (

    <Tab.Navigator>
      <Tab.Screen name="Tracklistflow" component={TrackList} />
      <Tab.Screen name="TrackCreate" component={TrackCreateScreen}></Tab.Screen>
      <Tab.Screen name="Account" component={AccountScreen}></Tab.Screen>
      <Tab.Screen name="Track detail" component={TrackDetailscreen}></Tab.Screen>
    </Tab.Navigator>

  );
};

export default App;

// export default ()=>{
//   return(
//         <AuthProvider>
//           <App/>
//         </AuthProvider>
//   );
// };


